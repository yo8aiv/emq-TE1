if [[ $EUID -ne 0 ]]; then
	whiptail --title "emq-TE1" --msgbox "Debe ejecutar este script como usuario ROOT" 0 50
	exit 0
fi

#!/bin/sh

apps=("git" "sudo" "curl" "wget" "sed" "wavemon")

for app in "${apps[@]}"
do
    # Verificar apps
    if ! dpkg -s "$app" >/dev/null 2>&1; then
        # app no instalada
        apt-get install -y "$app"
    else
        # app ya instalada
        echo "$app ya instalada"
    fi
done
####################
sudo cat > /opt/emq-ver <<- "EOF"
EMQ-VER:  01.05
EOF
####################
sudo timedatectl set-timezone America/Panama
####################################################################################
#                                swap raspberry
####################################################################################
if [ "$(cat /proc/cpuinfo | grep 'Raspberry')" != "" ]; then
sudo systemctl stop dphys-swapfile.service
sudo systemctl disable dphys-swapfile.service
sudo update-rc.d dphys-swapfile remove
sudo chmod -x /etc/init.d/dphys-swapfile
sudo dphys-swapfile swapoff
sudo swapoff -a
sudo rm /var/swap
sudo dphys-swapfile uninstall
sudo sed -i 's/CONF_SWAPSIZE=.*/CONF_SWAPSIZE=0/' /etc/dphys-swapfile

fi
#
#####################################################################################################################
#                                      rust
#####################################################################################################################
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs > rustup.sh
chmod +x rustup.sh
./rustup.sh -y
source "$HOME/.cargo/env"
rustup update
rustup update stable
#rustc --version
######################################################################################################################
#                                                           Cronedit
######################################################################################################################
cat > /usr/local/bin/cronedit.sh <<- "EOF"
cronjob_editor () {
# usage: cronjob_editor '<interval>' '<command>' <add|remove>

if [[ -z "$1" ]] ;then printf " no interval specified\n" ;fi
if [[ -z "$2" ]] ;then printf " no command specified\n" ;fi
if [[ -z "$3" ]] ;then printf " no action specified\n" ;fi

if [[ "$3" == add ]] ;then
    # add cronjob, no duplication:
    ( sudo crontab -l | grep -v -F -w "$2" ; echo "$1 $2" ) | sudo crontab -
elif [[ "$3" == remove ]] ;then
    # remove cronjob:
    ( sudo crontab -l | grep -v -F -w "$2" ) | sudo crontab -
fi
}
cronjob_editor "$1" "$2" "$3"


EOF
sudo chmod +x /usr/local/bin/cronedit.sh
####################
echo iniciando instalacion

bash -c "$(curl -fsSL https://gitlab.com/hp3icc/emq-TE1/-/raw/main/install/rm.sh)"
bash -c "$(curl -fsSL https://gitlab.com/hp3icc/emq-TE1/-/raw/main/install/dvswitch.sh)"
bash -c "$(curl -fsSL https://gitlab.com/hp3icc/emq-TE1/-/raw/main/install/direwolf.sh)"
#bash -c "$(curl -fsSL https://gitlab.com/hp3icc/emq-TE1/-/raw/main/install/pymultimonng.sh)"
bash -c "$(curl -fsSL https://gitlab.com/hp3icc/emq-TE1/-/raw/main/install/ysfreflector.sh)"
bash -c "$(curl -fsSL https://gitlab.com/hp3icc/emq-TE1/-/raw/main/install/mmdvm.sh)"
bash -c "$(curl -fsSL https://gitlab.com/hp3icc/emq-TE1/-/raw/main/install/ysfgateway2.sh)"
bash -c "$(curl -fsSL https://gitlab.com/hp3icc/emq-TE1/-/raw/main/install/fdmr.sh)"
bash -c "$(curl -fsSL https://gitlab.com/hp3icc/emq-TE1/-/raw/main/install/gotty.sh)"
bash -c "$(curl -fsSL https://gitlab.com/hp3icc/emq-TE1/-/raw/main/install/noip.sh)"
bash -c "$(curl -fsSL https://gitlab.com/hp3icc/python-aprs-beacon/-/raw/main/install.sh)"
bash -c "$(curl -fsSL https://gitlab.com/hp3icc/emq-TE1/-/raw/main/install/rebooters.sh)"
bash -c "$(curl -fsSL https://gitlab.com/hp3icc/emq-TE1/-/raw/main/install/ysf2dmr.sh)"
bash -c "$(curl -fsSL https://gitlab.com/hp3icc/emq-TE1/-/raw/main/install/dmrgw.sh)"
bash -c "$(curl -fsSL https://gitlab.com/hp3icc/emq-TE1/-/raw/main/install/f2b+ufw.sh)"
bash -c "$(curl -fsSL https://gitlab.com/hp3icc/python-aprs-wx/-/raw/main/install.sh)"
bash -c "$(curl -fsSL https://gitlab.com/hp3icc/emq-TE1/-/raw/main/install/raspberry.sh)"

###########################
echo install menu
bash -c "$(curl -fsSL https://gitlab.com/hp3icc/emq-TE1/-/raw/main/menu/menu)"
bash -c "$(curl -fsSL https://gitlab.com/hp3icc/emq-TE1/-/raw/main/menu/menu-aprs)"
bash -c "$(curl -fsSL https://gitlab.com/hp3icc/python-aprs-beacon/-/raw/main/menu.sh)"
bash -c "$(curl -fsSL https://gitlab.com/hp3icc/emq-TE1/-/raw/main/menu/menu-dvs)"
bash -c "$(curl -fsSL https://gitlab.com/hp3icc/emq-TE1/-/raw/main/menu/menu-dw)"
bash -c "$(curl -fsSL https://gitlab.com/hp3icc/emq-TE1/-/raw/main/menu/menu-fdmr)"
#bash -c "$(curl -fsSL https://gitlab.com/hp3icc/emq-TE1/-/raw/main/menu/menu-mm-rtl)"
bash -c "$(curl -fsSL https://gitlab.com/hp3icc/emq-TE1/-/raw/main/menu/menu-mmdvm)"
bash -c "$(curl -fsSL https://gitlab.com/hp3icc/emq-TE1/-/raw/main/menu/menu-noip)"
bash -c "$(curl -fsSL https://gitlab.com/hp3icc/emq-TE1/-/raw/main/menu/menu-reboot)"
bash -c "$(curl -fsSL https://gitlab.com/hp3icc/emq-TE1/-/raw/main/menu/menu-web)"
bash -c "$(curl -fsSL https://gitlab.com/hp3icc/emq-TE1/-/raw/main/menu/menu-wifi)"
bash -c "$(curl -fsSL https://gitlab.com/hp3icc/emq-TE1/-/raw/main/menu/menu-ysf)"
bash -c "$(curl -fsSL https://gitlab.com/hp3icc/emq-TE1/-/raw/main/menu/menu-ysf2dmr)"
bash -c "$(curl -fsSL https://gitlab.com/hp3icc/emq-TE1/-/raw/main/menu/menu-ufw)"

############################
(crontab -l; echo "* */1 * * * sync ; echo 3 > /proc/sys/vm/drop_caches >/dev/null 2>&1")|awk '!x[$0]++'|crontab -
sudo systemctl enable gotty.service
sudo systemctl stop webproxy.service
sudo systemctl disable webproxy.service

#############################################################################################################################
cat > /tmp/completado.sh <<- "EOF"
#!/bin/bash
while : ; do
choix=$(whiptail --title "Raspbian Proyect HP3ICC Esteban Mackay 73." --menu " Precione enter (return o intro) para finalizar la instalacion y reiniciar" 11 85 3 \
1 " Iniciar Reinicio de equipo " 3>&1 1>&2 2>&3)
exitstatus=$?
#on recupere ce choix
#exitstatus=$?
if [ $exitstatus = 0 ]; then
    echo "Your chosen option:" $choix
else
    echo "You chose cancel."; break;
fi
# case : action en fonction du choix
case $choix in
1)
sudo reboot
;;
esac
done
exit 0
EOF
###
sudo chmod +x /tmp/completado.sh
history -c && history -w
sh /tmp/completado.sh

 

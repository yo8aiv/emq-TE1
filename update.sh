#!/bin/bash
sudo cat > /bin/menu-update0 <<- "EOF"
#!/bin/bash
while : ; do
choix=$(whiptail --title "Raspbian Proyect HP3ICC Menu Update" --menu "Nota Importante: solo actualice aplicaciones que esten en uso, al finalizar la actualizacion la aplicacion se reiniciara, tenga pleno dominio de las configuraciones de cada aplicacion, antes de actualizar.
" 17 50 5 \
1 " ADVANCE UPGRADE & INSTALL " \
2 " Menu Principal " 3>&1 1>&2 2>&3)
exitstatus=$?
#on recupere ce choix
#exitstatus=$?
if [ $exitstatus = 0 ]; then
    echo "Your chosen option:" $choix
else
    echo "You chose cancel."; break;
fi
# case : action en fonction du choix
case $choix in
1)
menu-update2 ;;
2)
break;
esac
done
exit 0

EOF
####
sudo cat > /bin/menu-update2 <<- "EOF"
#!/bin/bash
while : ; do
choix=$(whiptail --title "Raspbian Proyect HP3ICC Menu Avance Upgrade" --menu "Nota Importante: solo actualice aplicaciones que esten en uso, al finalizar la actualizacion la aplicacion se reiniciara, tenga pleno dominio de las configuraciones de cada aplicacion, antes de actualizar.
" 21 50 8 \
1 " UPGRADE to Reuter AP Mode " \
2 " install DVSMU " \
3 " reinstall raspi-config " \
4 " Menu Principal " 3>&1 1>&2 2>&3)
exitstatus=$?
#on recupere ce choix
#exitstatus=$?
if [ $exitstatus = 0 ]; then
    echo "Your chosen option:" $choix
else
    echo "You chose cancel."; break;
fi
# case : action en fonction du choix
case $choix in
1)
bash -c "$(curl -fsSL https://gitlab.com/hp3icc/emq-TE1/-/raw/main/te1ap.sh)" ;;
2)
bash -c "$(curl -fsSL https://gitlab.com/hp3icc/DVSMU/-/raw/main/emq-dvsmu.sh)" ;;
3)
if [ "$(cat /proc/cpuinfo | grep 'Raspberry')" != "" ]; then
apt-get update
sudo apt-get install raspi-config -y
else
  whiptail --title "emq-TE1" --msgbox "Solo para Raspberry / Only for Raspberry" 0 50
	exit 0
fi;;
4)
break;
esac
done
exit 0

EOF
#
#!/bin/bash
cd /
#


##########################################  End Update Files   ##############################################################
sudo chmod +x /bin/menu*
menu-update0

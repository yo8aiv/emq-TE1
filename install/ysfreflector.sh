#!/bin/bash
#########################################################################################################################
#                                        YSF-Reflector
#########################################################################################################################

apps=("python3" "python3-pip")

for app in "${apps[@]}"
do
    # Verificar apps
    if ! dpkg -s "$app" >/dev/null 2>&1; then
        # app no instalada
        sudo apt-get install -y "$app"
    else
        # app ya instalada
        echo "$app ya instalada"
    fi
done

pip3 install --force-reinstall aprslib
pip3 install --force-reinstall tinydb

if [ -d "/var/log/mmdvm" ]
then
   rm -r /var/log/mmdvm
fi
   mkdir /var/log/mmdvm

if [ -d "/opt/pYSFReflector3" ]
then
   rm -r /opt/pYSFReflector3
fi


sudo groupadd mmdvm
sudo useradd mmdvm -g mmdvm -s /sbin/nologin
#sudo chown -R mmdvm:mmdvm /var/log/YSFReflector 

cd /opt
git clone https://github.com/iu5jae/pYSFReflector3.git
cd pYSFReflector3/
sudo chmod +x /opt/pYSFReflector3/*.py
sudo chmod +x /opt/pYSFReflector3/YSFReflector
#sudo sed -i 's/mmdvm/YSFReflector/' /opt/pYSFReflector3/pysfreflector.ini
sudo sed -i 's/0.0.0.0//' /opt/pYSFReflector3/pysfreflector.ini
sudo sed -i 's/pysfreflector/pYSFReflector3/' /opt/pYSFReflector3/pysfreflector.ini
sudo sed -i 's/enable =.*/enable = 1/' /opt/pYSFReflector3/pysfreflector.ini
sudo sed -i 's/aprs.grupporadiofirenze.net/noam.aprs2.net/' /opt/pYSFReflector3/pysfreflector.ini
sudo sed -i 's/ssid =.*/ssid = -7/' /opt/pYSFReflector3/pysfreflector.ini
sudo sed -i "s/FileLevel=.*/FileLevel=0/g" /opt/pYSFReflector3/pysfreflector.ini
sudo sed -i "s/list =.*/list = /g" /opt/pYSFReflector3/pysfreflector.ini
sudo sed -i "s/default =.*/default = /g" /opt/pYSFReflector3/pysfreflector.ini
sudo sed -i "s/local =.*/local = /g" /opt/pYSFReflector3/pysfreflector.ini
sudo sed -i "s/prefix =.*/prefix = 0/g" /opt/pYSFReflector3/pysfreflector.ini
sudo sed -i "s/Timeout =.*/Timeout = 240/g" /opt/pYSFReflector3/pysfreflector.ini
sudo sed -i "s/FilePath=.*/FilePath=\/var\/log\/mmdvm/g" /opt/pYSFReflector3/pysfreflector.ini
#
sudo cat > /lib/systemd/system/YSFReflector.service <<- "EOF"
[Unit]
Description=YSFReflector
After=multi-user.target

[Service]
#User=root
#ExecStartPre=/bin/sleep 30
ExecStart=/usr/bin/python3 /opt/pYSFReflector3/YSFReflector /opt/pYSFReflector3/pysfreflector.ini
Restart=on-failure

[Install]
WantedBy=multi-user.target

EOF
#########################################################################################################################
#                                        YSF-Reflector-Dashboard
#########################################################################################################################

apps=("python3-websockets" "python3-psutil")

for app in "${apps[@]}"
do
    # Verificar apps
    if ! dpkg -s "$app" >/dev/null 2>&1; then
        # app no instalada
        sudo apt-get install -y "$app"
    else
        # app ya instalada
        echo "$app ya instalada"
    fi
done

if ! command -v ansi2html &> /dev/null; then
    pip3 install ansi2html
fi

cd /opt/
git clone --recurse-submodules -j8 https://github.com/dg9vh/WSYSFDash
cd /opt/WSYSFDash/
sudo chmod +x /opt/WSYSFDash/*
sudo chown -R root /opt/WSYSFDash

#
variable2=$(date +'%Y' | tail -c 5)
sudo sed -i "s/switch theme.*/switch theme<\/a><\/span>  <\/div> <p style=\"text-align: center;\"><span class=\"text-muted\"><a title=\"Raspbian Proyect by HP3ICC © 2018-$variable2\" target=\"_blank\" href=https:\/\/gitlab.com\/hp3icc\/emq-TE1\/>Proyect: emq-TE1+<\/a><\/span>/g" /opt/WSYSFDash/html/index.html
sudo sed -i "s/connectedsincecol = 1;/connectedsincecol = 0;/g" /opt/WSYSFDash/html/js/config.js
sudo sed -i "s/\/usr\/local\/bin\/YSFReflector/\/opt\/pYSFReflector3\/YSFReflector/g" /opt/WSYSFDash/logtailer.ini
sudo sed -i "s/Filerotate=True/Filerotate=False/g" /opt/WSYSFDash/logtailer.ini
sudo sed -i "s/YSFReflector1/mmdvm/g" /opt/WSYSFDash/logtailer.ini
sudo sed -i "s/lastheard = 2;/lastheard = 1;/g" /opt/WSYSFDash/html/js/config.js
sudo sed -i "s/mutedgateways = 1;/mutedgateways = 0;/g" /opt/WSYSFDash/html/js/config.js
sudo sed -i "s/gateways = 1;/gateways = 2;/g" /opt/WSYSFDash/html/js/config.js
sudo sed -i "s/voicemodecol = 1;/voicemodecol = 0;/g" /opt/WSYSFDash/html/js/config.js
sudo sed -i "s/datatable_default_length = 10;/datatable_default_length = 25;/g" /opt/WSYSFDash/html/js/config.js
sudo sed -i "s/useDarkTheme = 0;/useDarkTheme = 1;/g" /opt/WSYSFDash/html/js/config.js
sudo sed -i "s/qso = 1;/qso = 0;/g" /opt/WSYSFDash/html/js/config.js
sudo sed -i "s/about = 1;/about = 0;/g" /opt/WSYSFDash/html/js/config.js
sudo sed -i "s/coordscol = 1;/coordscol = 0;/g" /opt/WSYSFDash/html/js/config.js
sudo sed -i "s/osm = 1;/osm = 0;/g" /opt/WSYSFDash/html/js/config.js
sudo sed -i "s/gatewayipcol = 1;/gatewayipcol = 0;/g" /opt/WSYSFDash/html/js/config.js
sudo sed -i "s/col-10/col-9/g" /opt/WSYSFDash/html/index.html
sudo sed -i "s/Here you can place your individual Headline/pYSFReflector 3/g" /opt/WSYSFDash/html/index.html
sudo sed -i "s/You can also disable the complete section within the index.html by searching for \"Header-Section\"-comment-lines and remove it from the source code./YSF Reflector with APRS.fi RX-IGate/g" /opt/WSYSFDash/html/index.html
sudo sed -i "s/Here is the place to put some nice stuff of text you want to have placed here. You can use all html you like./Python Reflector IU5JAE, emq-TE1ws Raspbian Proyect by hp3icc./g" /opt/WSYSFDash/html/index.html
sudo sed -i "s/The image file for the logo is placed in the folder \"\/html\/img\" with the name \"logo.jpg\" - feel free to replace it with yours or replace the URL for the image with your own./ /g" /opt/WSYSFDash/html/index.html
wget https://cdn-bio.qrz.com/c/hp3icc/ysf_logo_sq_243px.jpg -O /opt/WSYSFDash/html/img/logo.jpg

#
cat > /lib/systemd/system/http.server-ysf.service <<- "EOF"
[Unit]
Description=Python3 http.server-ysf
After=network.target

[Service]
#User=root
#ExecStartPre=/bin/sleep 45
#Type=simple
#User=mmdvm
#Group=mmdvm
#Restart=always
# Modify for different location of Python3 or other port
ExecStart=/usr/bin/python3 -m http.server 80 --directory /opt/WSYSFDash/html
Restart=on-failure

[Install]
WantedBy=multi-user.target

EOF
#
cat > /lib/systemd/system/logtailer-ysf.service <<- "EOF"
[Unit]
Description=Python3 logtailer for WSYSFDash
After=network.target

[Service]
#ExecStartPre=/bin/sleep 10
#Type=simple
#User=mmdvm
#Group=mmdvm
#Restart=always
# Modify for different location of Python3 or location of files
WorkingDirectory=/opt/WSYSFDash/
ExecStart=/usr/bin/python3 /opt/WSYSFDash/logtailer.py
Restart=on-failure


[Install]
WantedBy=multi-user.target


EOF
#
systemctl daemon-reload

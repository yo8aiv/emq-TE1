#!/bin/bash
#####################################################################################################################
#                                            rebooter general system
#####################################################################################################################

sudo cat > /usr/local/bin/rebooter1.sh <<- "EOF"
#!/bin/bash

# Tiempo de monitoreo en segundos
TIEMPO_MONITOREO=30

# Tiempo de espera entre los chequeos en segundos
TIEMPO_ESPERA=60

# Función para verificar el estado de Internet
verificar_internet() {
  ping -c 1 noip.com > /dev/null 2>&1
  return $?
}

# Bucle infinito para monitorear
while true; do
  if verificar_internet; then
    # Acción silenciosa
    :
    echo "internet ok"
  else
#    echo "sin internet"
    sleep $TIEMPO_MONITOREO

    if verificar_internet; then
      # Conexión restablecida
#     echo "internet restablecido"
      :
    else
      sudo reboot
#      echo "apliando reboot"
    fi
  fi

  sleep $TIEMPO_ESPERA
done


EOF
#
#####################################################################################################################
#                                            rebooter 2
#####################################################################################################################

sudo cat > /usr/local/bin/rebooter2.sh <<- "EOF"
#!/bin/bash
sleep 180
while :
do
SERVER=noip.com

ping -c1 ${SERVER} > /dev/null

if [ $? != 0 ]
then
#
sudo reboot

fi


  sleep 60
done

EOF
#
sudo cat > /lib/systemd/system/rebooter1.service  <<- "EOF"
[Unit]
Description=Rebooter Service1
After=network.target

[Service]
ExecStart=/usr/local/bin/rebooter1.sh
Restart=on-failure

[Install]
WantedBy=default.target

EOF
#
sudo cat > /lib/systemd/system/rebooter2.service  <<- "EOF"
[Unit]
Description=Rebooter Service2
After=network.target

[Service]
ExecStart=/usr/local/bin/rebooter2.sh
Restart=on-failure

[Install]
WantedBy=default.target

EOF
#########
sudo systemctl daemon-reload
sudo chmod +x /usr/local/bin/rebooter*

#!/bin/bash
#############################################################################################################################
#                                                           dvswitch
#############################################################################################################################

bash -c "$(curl -fsSL https://gitlab.com/hp3icc/Easy-DVSwitch/-/raw/main/install.sh)"

sudo cat > /usr/local/dvs/dvs <<- "EOF"
#!/bin/bash

#===================================
SCRIPT_VERSION="Menu Script v.1.61"
SCRIPT_AUTHOR="HL5KY"
SCRIPT_DATE="11/06/2020"
#===================================

source /var/lib/dvswitch/dvs/var.txt

if [ "$1" != "" ]; then
    case $1 in
        -v|-V|--version) echo "dvs "$SCRIPT_VERSION; exit 0 ;;
        -a|-A|--author) echo "dvs "$SCRIPT_AUTHOR; exit 0 ;;
        -d|-D|--date) echo "dvs "$SCRIPT_DATE; exit 0 ;;
          *) echo "Unknown parameter passed: $1"; exit 1 ;;
    esac
fi

#--------------------------------------------------------------------

clear

# After upgrading, if [there is dvsm.basic] -> meaning setting is Advanced Macro Configuration
if [ -e ${AB}dvsm.basic ]; then
#    if there is not character "Advanced" in dvsm.macro -> updated & upgraded and dvsm.macro is brand new
        if [[ -z `grep "Advanced" ${AB}dvsm.macro` ]]; then
                sudo \cp -f ${adv}dvsm.macro ${AB}dvsm.macro
        fi
fi


if [ -e /var/lib/dvswitch/dvs/var.old ]; then
clear
sudo \mv -f /var/lib/dvswitch/dvs/var.old /var/lib/dvswitch/dvs/var.txt
source /var/lib/dvswitch/dvs/var.txt
fi


if [ ! -e ${lan}language.txt ]; then
clear
sudo \cp -f ${lan}english.txt ${lan}language.txt
source /var/lib/dvswitch/dvs/var.txt
fi


if [ "$startup_lan" != "73" ]; then

clear

update_var startup_lan 73

	if (whiptail --title " Change Language Settings " --yesno "\

           Do you want to change Language settings now ?


           You can do it later. The menu is under <Tools>
	" 12 70); then
	${DVS}language.sh; exit 0
	fi
fi


#--------------------------------------------------------------------

OPTION=$(whiptail --title " $T010 " --menu "\
                                           $SCRIPT_VERSION\n
\n
" 14 110 6 \
"01 $T011     " "$T012" \
"02 $T013     " "$T014" \
"03 $T015     " "$T016" \
"04 $T023     " "$T024" \
"05 $T017     " "$T018" \
"06 $T019     " "$T020" 3>&1 1>&2 2>&3)

if [ $? != 0 ]; then
clear;
exit 0
fi

case $OPTION in
01\ *)sudo ${DVS}init_config.sh ;;
02\ *)sudo ${DVS}adv_config_menu.sh ;;
03\ *)sudo ${DVS}tools_menu.sh ;;
04\ *)bash -c "$(curl -fsSL https://gitlab.com/hp3icc/emq-TE1/-/raw/main/service/dv-list-tg.sh)" ;;
05\ *)sudo ${DVS}credits.sh ;;
06\ *)exit 0
esac

#exit 0

EOF
###
cd /tmp/
wget https://gitlab.com/hp3icc/DVSwitch-Mobile-TG-List/-/raw/main/lang.sh
sudo chmod +x lang.sh
sh lang.sh
###
sed -i "s/default_dmr_server=.*/default_dmr_server=FreeDMR/g"  /var/lib/dvswitch/dvs/var.txt
sed -i "s/other1_name=.*/other1_name=FreeDMR/g"  /var/lib/dvswitch/dvs/var.txt
sed -i "s/other1_address=.*/other1_address=freedmr-hp.ddns.net/g"  /var/lib/dvswitch/dvs/var.txt
sed -i "s/other1_password=.*/other1_password=passw0rd/g"  /var/lib/dvswitch/dvs/var.txt
sed -i "s/other1_port=.*/other1_port=62031/g"  /var/lib/dvswitch/dvs/var.txt

sed -i "s/other2_name=.*/other2_name=DMR-Central/g"  /var/lib/dvswitch/dvs/var.txt
sed -i "s/other2_address=.*/other2_address=dmr.pa7lim.nl/g"  /var/lib/dvswitch/dvs/var.txt
sed -i "s/other2_password=.*/other2_password=passw0rd/g"  /var/lib/dvswitch/dvs/var.txt
sed -i "s/other2_port=.*/other2_port=55555/g"  /var/lib/dvswitch/dvs/var.txt
sed -i "s/42000/42500/g" /opt/YSFGateway/YSFGateway.ini
sed -i "s/42001/43001/g" /opt/YSFGateway/YSFGateway.ini
sed -i "s/RptPort=3200/RptPort=3230/g" /opt/YSFGateway/YSFGateway.ini
sed -i "s/LocalPort=4200/LocalPort=4230/g" /opt/YSFGateway/YSFGateway.ini
sudo mkdir /var/www/dvs
sudo mv /var/www/html/* /var/www/dvs/
sed -i "s/<br>Dashboard based on Pi-Star Dashboard, © Andy Taylor.*/<br>Dashboard based on Pi-Star Dashboard, © Andy Taylor (MW0MWZ) and adapted to DVSwitch by SP2ONG<br> <a title=\"Raspbian Proyect by HP3ICC © <?php \$cdate=date(\"Y\"); if (\$cdate > \"2018\") {\$cdate=\"2018-\".date(\"Y\");} echo \$cdate; ?>\" target=\"_blank\" href=https:\/\/gitlab.com\/hp3icc\/emq-TE1\/>Proyect: emq-TE1ws+<\/a><\/span><\/center>/" /var/www/dvs/index.php
sed -i 's/www\/html/www\/dvs/g' /usr/local/sbin/update-config.sh
sed -i 's/www\/html/www\/dvs/g' /var/lib/dpkg/info/dvswitch*

sed -i "s/Language=en_US/Language=es_ES/g" /opt/NXDNGateway/NXDNGateway.ini
sed -i "s/Language=en_US/Language=es_ES/g" /opt/P25Gateway/P25Gateway.ini 

#
cat > /var/www/dvs/include/lh.php  <<- "EOFX8"
<?php
include_once $_SERVER['DOCUMENT_ROOT'].'/include/config.php';         
include_once $_SERVER['DOCUMENT_ROOT'].'/include/tools.php';        
include_once $_SERVER['DOCUMENT_ROOT'].'/include/functions.php';    
?>
<span style="font-weight: bold;font-size:14px;">Gateway Activity</span>
<fieldset style="box-shadow:0 0 10px #999;background-color:#e8e8e8e8; width:660px;margin-top:10px;margin-left:0px;margin-right:0px;font-size:12px;border-top-left-radius: 10px; border-top-right-radius: 10px;border-bottom-left-radius: 10px; border-bottom-right-radius: 10px;">
  <table style="margin-top:3px;">
    <tr>
      <th>Time (<?php echo date('T')?>)</th>
      <th>Mode</th>
      <th>Callsign</th>
	  <th>Name</th>
      <th>Target</th>
      <th>Src</th>
      <th>Dur(s)</th>
      <th>Loss</th>
      <th>BER</th>
    </tr>
<?php
$i = 0;
$a = 0;
$dmrCall = 0;
$dmrIDline = file(DMRIDDATPATH."/DMRIds.dat");
$countDMRId = count($dmrIDline);
for ($i = 0;  ($i <= 19); $i++) { //Last 20 calls
	if (isset($lastHeard[$i])) {
		$listElem = $lastHeard[$i];
		if ( $listElem[2] ) {
			$utc_time = $listElem[0];
                        $utc_tz =  new DateTimeZone('UTC');
                        $local_tz = new DateTimeZone(date_default_timezone_get ());
                        $dt = new DateTime($utc_time, $utc_tz);
                        $dt->setTimeZone($local_tz);
                        $local_time = strftime('%H:%M:%S %b %d', $dt->getTimestamp());

		echo"<tr>";
		echo"<td align=\"left\">&nbsp;$local_time</td>";
		echo"<td align=\"left\" style=\"color:green; font-weight:bold;\">&nbsp;$listElem[1]</td>";
		if ((is_numeric($listElem[2]) || strpos($listElem[2], "openSPOT") !== FALSE) && (strlen($listElem[2])==7)) {
		    echo "<td align=\"left\" style=\"color:#464646;\">&nbsp;<a href=\"https://database.radioid.net/database/view?id=$listElem[2]\" target=\"_blank\"><span style=\"color:#464646;font-weight:bold;\">$listElem[2]</span></a></td>";
		} elseif (!preg_match('/[A-Za-z].*[0-9]|[0-9].*[A-Za-z]/', $listElem[2])|| $listElem[2] == "N0CALL") {
 	                       echo "<td align=\"left\" style=\"color:#464646;\"><b>&nbsp;$listElem[2]</b></td>";
		} else {
		    if (strpos($listElem[2],"-") > 0) { $listElem[2] = substr($listElem[2], 0, strpos($listElem[2],"-")); }
			    if ( $listElem[3] && $listElem[3] != '    ' ) {
			echo "<td align=\"left\">&nbsp;<a href=\"http://www.qrz.com/db/$listElem[2]\" target=\"_blank\"><b>$listElem[2]</b></a><span style=\"color:#464646;font-weight:bold;\">/$listElem[3]</span></td>";
		    } else {
			echo "<td align=\"left\">&nbsp;<a href=\"http://www.qrz.com/db/$listElem[2]\" target=\"_blank\"><b>$listElem[2]</b></a></td>";
		    }
		}
		$arr2 = strtoupper($listElem[2]);
		for ($a = 0; ($a < $countDMRId); $a++) {
			if (strpos($dmrIDline[$a], $arr2." ") != false) {
				$arr = explode(" ", strtoupper($dmrIDline[$a]));
				if ($arr[1] == $arr2) {
					echo "<td align=\"left\" style=\"color:red; font-weight:bold;\">&nbsp;<b>$arr[2]</b></a></td>";
					$dmrCall = 1;
					break;
				}
			}
		}
		if ($dmrCall == 0) { echo "<td align=\"left\" style=\"color:red; font-weight:bold;\">&nbsp;<b></b></a></td>"; }
		if (strlen($listElem[4]) == 1) { $listElem[4] = str_pad($listElem[4], 8, " ", STR_PAD_LEFT); }
		if ( substr($listElem[4], 0, 6) === 'CQCQCQ' ) {
			echo "<td align=\"left\">&nbsp;<span style=\"color:#b5651d;font-weight:bold;\">$listElem[4]</span></td>";
		} else {
			echo "<td align=\"left\">&nbsp;<span style=\"color:#b5651d;font-weight:bold;\">".str_replace(" ","&nbsp;", $listElem[4])."</span></td>";
		}


if ($listElem[5] == "LNet"){
			echo "<td style=\"background:#1d1;\">LNet</td>";
		}else{
			echo "<td>$listElem[5]</td>";
		}
		if ($listElem[6] == null) {
                             if ($listElem[1] == "DMR Slot 2" && $listElem[5] == "Net")  {echo "<td colspan=\"3\" style=\"background:#f93;\">&nbsp;&nbsp;&nbsp;RX DMR&nbsp;&nbsp;&nbsp;</td>";}
                             if ($listElem[1] == "DMR Slot 1" && $listElem[5] == "Net")  {echo "<td colspan=\"3\" style=\"background:#f93;\">&nbsp;&nbsp;&nbsp;RX DMR&nbsp;&nbsp;&nbsp;</td>";}
                             if ($listElem[1] == "YSF" && $listElem[5] == "Net")  {echo "<td colspan=\"3\" style=\"background:#ff9;\">&nbsp;&nbsp;&nbsp;RX YSF&nbsp;&nbsp;&nbsp;</td>";}
                             if ($listElem[1] == "P25" && $listElem[5] == "Net")  {echo "<td colspan=\"3\" style=\"background:#f9f;\">&nbsp;&nbsp;&nbsp;RX P25&nbsp;&nbsp;&nbsp;</td>";}
			     if ($listElem[1] == "NXDN" && $listElem[5] == "Net")  {echo "<td colspan=\"3\" style=\"background:#c9f;\">&nbsp;&nbsp;&nbsp;RX NXDN&nbsp;&nbsp;</td>";}
                             if ($listElem[1] == "D-Star" && $listElem[5] == "Net")  {echo "<td colspan=\"3\" style=\"background:#ade;\">&nbsp;&nbsp;&nbsp;RX D-Star</td>";}
                             if ($listElem[5] == "LNet")  {echo "<td colspan=\"3\" style=\"background:#f33;\">TX</td>";}
			} else if ($listElem[6] == "DMR Data") {
				echo "<td colspan=\"3\" style=\"background:#1d1;\">DMR Data</td>";
			} else if ($listElem[6] == "GPS") {
				echo "<td colspan=\"3\" style=\"background:#1d1;\"><a style=\"display:block;\" target=\"_blank\" href=https://www.openstreetmap.org/?mlat=".floatval($listElem[9])."&mlon=".floatval($listElem[10])."><b>GPS</b></a></td>";
			} else {
			echo "<td>$listElem[6]</td>";
			// Colour the Loss Field
			if (floatval($listElem[7]) < 1) { echo "<td>$listElem[7]</td>"; }
			elseif (floatval($listElem[7]) == 1) { echo "<td style=\"background:#1d1;\">$listElem[7]</td>"; }
			elseif (floatval($listElem[7]) > 1 && floatval($listElem[7]) <= 3) { echo "<td style=\"background:#fa0;\">$listElem[7]</td>"; }
			else { echo "<td style=\"background:#f33;color:#f9f9f9;\">$listElem[7]</td>"; }
			// Colour the BER Field
			if (floatval($listElem[8]) == 0) { echo "<td>$listElem[8]</td>"; }
			elseif (floatval($listElem[8]) >= 0.0 && floatval($listElem[8]) <= 1.9) { echo "<td style=\"background:#1d1;\">$listElem[8]</td>"; }
			elseif (floatval($listElem[8]) >= 2.0 && floatval($listElem[8]) <= 4.9) { echo "<td style=\"background:#fa0;\">$listElem[8]</td>"; }
			else { echo "<td style=\"background:#f33;color:#f9f9f9;\">$listElem[8]</td>"; }
		}
		echo"</tr>\n";
                $dmrCall = 0;
		}
	}
}
?>
  </table>
</fieldset>


EOFX8
#
sed -i "s/utc_tz =  new DateTimeZone('UTC'/utc_tz =  new DateTimeZone('Africa\/Lagos'/"  /var/www/dvs/include/localtx.php
sed -i "s/utc_tz =  new DateTimeZone('UTC'/utc_tz =  new DateTimeZone('Africa\/Lagos'/"  /var/www/dvs/include/lh.php

#
cat > /opt/MMDVM_Bridge/MMDVM_Bridge.ini  <<- "EOF"
[General]
Callsign=N0CALL
Id=1234567
Timeout=300
Duplex=0

[Info]
RXFrequency=147000000
TXFrequency=147000000
Power=1
Latitude=8.5211
Longitude=-80.3598
Height=0
Location=DVSwitch Server
Description=MMDVM DVSwitch
URL=https://groups.io/g/DVSwitch

[Log]
# Logging levels, 0=No logging, 1=Debug, 2=Message, 3=Info, 4=Warning, 5=Error, 6=Fatal
DisplayLevel=1
FileLevel=1
FilePath=/var/log/mmdvm
FileRoot=MMDVM_Bridge

[DMR Id Lookup]
File=/var/lib/mmdvm/DMRIds.dat
Time=24

[NXDN Id Lookup]
File=/var/lib/mmdvm/NXDN.csv
Time=24

[Modem]
Port=/dev/null
RSSIMappingFile=/dev/null
Trace=0
Debug=0

[D-Star]
Enable=0
Module=B

[DMR]
Enable=0
ColorCode=1
EmbeddedLCOnly=1
DumpTAData=0

[System Fusion]
Enable=0

[P25]
Enable=0
NAC=293

[NXDN]
Enable=0
RAN=1
Id=12345

[D-Star Network]
Enable=0
GatewayAddress=127.0.0.1
GatewayPort=20010
LocalPort=20011
Debug=0

[DMR Network]
Enable=0
Address=hblink.dvswitch.org
Port=62031
Jitter=500
Local=62032
Password=passw0rd
# for DMR+ see https://github.com/DVSwitch/MMDVM_Bridge/blob/master/DOC/DMRplus_startup_options.md
# for XLX the syntax is: Options=XLX:4009
# No active linea de Option para TG estaticos, si utiliza BM,TGIF,DMR-Central
# Puede activar linea de option de selfcare FDMR-Mon y colocar su propia contraseña o 
# utilizar linea de options con opciones de tg estaticos
#Options=PASS=abc123
#Options=TS2=714,7144;DIAL=0;VOICE=0;LANG=es_ES;SINGLE=0;TIMER=10;
Slot1=0
Slot2=1
Debug=0

[System Fusion Network]
Enable=0
LocalAddress=0
LocalPort=3230
GatewayAddress=127.0.0.1
GatewayPort=4230
Debug=0

[P25 Network]
Enable=0
GatewayAddress=127.0.0.1
GatewayPort=42020
LocalPort=32010
Debug=0

[NXDN Network]
Enable=0
#LocalAddress=127.0.0.1
Debug=0
LocalPort=14021
GatewayAddress=127.0.0.1
GatewayPort=14020
EOF
####
sudo systemctl daemon-reload

sudo systemctl disable lighttpd.service
sudo systemctl stop lighttpd.service

sudo systemctl stop md380-emu.service
sudo systemctl stop analog_bridge.service
sudo systemctl stop mmdvm_bridge.service
sudo systemctl stop nxdngateway.service
sudo systemctl stop p25gateway.service
sudo systemctl stop ysfgateway.service
sudo systemctl stop quantar_bridge.service
sudo systemctl stop ircddbgatewayd.service
sudo systemctl stop p25parrot.service
sudo systemctl stop ysfparrot.service
sudo systemctl stop nxdnparrot.service

sudo systemctl disable md380-emu.service
sudo systemctl disable analog_bridge.service
sudo systemctl disable mmdvm_bridge.service
sudo systemctl disable nxdngateway.service
sudo systemctl disable p25gateway.service
sudo systemctl disable ysfgateway.service
sudo systemctl disable quantar_bridge.service
sudo systemctl disable ircddbgatewayd.service
sudo systemctl disable p25parrot.service
sudo systemctl disable ysfparrot.service
sudo systemctl disable nxdnparrot.service

rm /lib/systemd/system/analog_bridge.service
rm /lib/systemd/system/mmdvm_bridge.service
rm /lib/systemd/system/ysfgateway.service
rm /lib/systemd/system/ysfparrot.service
rm /lib/systemd/system/nxdngateway.service
rm /lib/systemd/system/nxdnparrot.service
rm /lib/systemd/system/p25gateway.service
rm /lib/systemd/system/p25parrot.service
rm /lib/systemd/system/quantar_bridge.service
rm /lib/systemd/system/ircddbgatewayd.service
rm /lib/systemd/system/md380-emu.service
cd /lib/systemd/system/
wget https://raw.githubusercontent.com/hp3icc/emq-TE1ws/main/service/analog_bridge.service
wget https://raw.githubusercontent.com/hp3icc/emq-TE1ws/main/service/mmdvm_bridge.service
wget https://raw.githubusercontent.com/hp3icc/emq-TE1ws/main/service/ysfgateway.service
wget https://raw.githubusercontent.com/hp3icc/emq-TE1ws/main/service/ysfparrot.service
wget https://raw.githubusercontent.com/hp3icc/emq-TE1ws/main/service/nxdngateway.service
wget https://raw.githubusercontent.com/hp3icc/emq-TE1ws/main/service/nxdnparrot.service
wget https://raw.githubusercontent.com/hp3icc/emq-TE1ws/main/service/p25gateway.service
wget https://raw.githubusercontent.com/hp3icc/emq-TE1ws/main/service/p25parrot.service
wget https://raw.githubusercontent.com/hp3icc/emq-TE1ws/main/service/quantar_bridge.service
wget https://raw.githubusercontent.com/hp3icc/emq-TE1ws/main/service/ircddbgatewayd.service
wget https://raw.githubusercontent.com/hp3icc/emq-TE1ws/main/service/md380-emu.service

rm /var/log/mmdvm/*

cat > /lib/systemd/system/http.server-dvs.service <<- "EOF"
[Unit]
Description=PHP http.server.DVS
After=network.target

[Service]
User=root
#ExecStartPre=/bin/sleep 30
# Modify for different other port
ExecStart=php -S 0.0.0.0:80 -t /var/www/dvs/

[Install]
WantedBy=multi-user.target



EOF
#
systemctl daemon-reload 

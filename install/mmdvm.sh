#!/bin/bash
#################################################################################################################################
#                                                      i2c
#################################################################################################################################

cd /boot
sudo sed -i 's/console=serial0,115200 //' /boot/cmdline.txt

sudo systemctl stop serial-getty@ttyAMA0.service
sudo systemctl stop bluetooth.service
sudo systemctl disable serial-getty@ttyAMA0.service
sudo systemctl disable bluetooth.service

archivo="/boot/config.txt"
palabra=("enable_uart=1" "dtoverlay=pi3-disable-bt" "dtparam=spi=on")

for palabra in "${palabra[@]}"
do

if grep -i -E "$palabra" "$archivo" >/dev/null 2>&1; then
   echo "line found"
 else
    echo "$palabra">> "$archivo"
fi
done

sudo sed -i 's/#dtparam=i2c_arm=on/dtparam=i2c_arm=on/' /boot/config.txt
sudo sed -i 's/dtparam=audio=on/#dtparam=audio=on/' /boot/config.txt
sudo sed -i 's/#dtparam=spi=on/dtparam=spi=on/' /boot/config.txt
sudo sed -i 's/#dtoverlay=pi3-disable-bt/dtoverlay=pi3-disable-bt/' /boot/config.txt
sudo sed -i 's/#enable_uart=1/enable_uart=1/' /boot/config.txt

cat > /lib/systemd/system/monp.service  <<- "EOF"
[Unit]
Description=sudo modprobe i2c-dev
#Wants=network-online.target
#After=syslog.target network-online.target

[Service]
#User=root
#ExecStartPre=/bin/sleep 1800
ExecStart=sudo modprobe i2c-dev
Restart=on-failure

[Install]
WantedBy=multi-user.target
EOF

sudo systemctl enable monp.service
#################################################################################################################################
#                                                        MMDVMHost
#################################################################################################################################

apps=("git" "make" "build-essential" "libusb-1.0-0-dev" "python" "python3" "python3-pip" "chkconfig" "git-core" "libi2c-dev" "i2c-tools" "lm-sensors")

for app in "${apps[@]}"
do
    # Verificar apps
    if ! dpkg -s "$app" >/dev/null 2>&1; then
        # app no instalada
        sudo apt-get install -y "$app"
    else
        # app ya instalada
        echo "$app ya instalada"
    fi
done

mkdir /var/log/mmdvmh

if [ -d "/var/log/mmdvmh" ]
then
  echo "found file"
else
   mkdir /var/log/mmdvmh
fi
if [ -d "/opt/MMDVMHost" ]
then
  rm -r /opt/MMDVMHost
fi

cd /opt
git clone https://github.com/g4klx/MMDVMHost.git
cd /opt/MMDVMHost
sudo make clean

git clone https://github.com/hallard/ArduiPi_OLED
cd ArduiPi_OLED
sudo make
cd /opt/MMDVMHost/
make clean
sudo make -f Makefile.Pi.OLED
sudo make install

groupadd mmdvm 
useradd mmdvm -g mmdvm -s /sbin/nologin 
chown mmdvm /var/log/
#######
cat > /lib/systemd/system/mmdvmh.service  <<- "EOF"
[Unit]
Description=MMDVM Host Service
After=syslog.target network.target

[Service]
#User=root
#Type=simple
#Restart=always
#RestartSec=3
#StandardOutput=null
WorkingDirectory=/opt/MMDVMHost
#ExecStartPre=/bin/sleep 15
ExecStart=/opt/MMDVMHost/MMDVMHost /opt/MMDVMHost/MMDVM.ini
Restart=on-failure

[Install]
WantedBy=multi-user.target

EOF
#

cat > /opt/MMDVMHost/MMDVM.ini  <<- "EOF"
[Service]
ServiceStart=0

[General]
# Coloque su indicativo
Callsign=HP3ICC
# Coloque su DMRID de 7 digitos mas 2 digitos para su conexion
Id=000000000
Timeout=300
Duplex=0
ModeHang=10
#RFModeHang=10
#NetModeHang=3
#Display=None
Display=OLED
#Display=Nextion
Daemon=0

[Info]
# Colocar frecuencia 9 digitos sin puntos
RXFrequency=433400000
TXFrequency=433400000
Power=1
# The following lines are only needed if a direct connection to a DMR master is being used
Latitude=0.0
Longitude=0.0
Height=0
Location=Panama
Description=emq-TE1-MMDVM
URL=https://gitlab.com/hp3icc/emq-TE1

[Log]
# Logging levels, 0=No logging
DisplayLevel=1
FileLevel=1
FilePath=/var/log/mmdvmh
FileRoot=MMDVMH
FileRotate=0

[CW Id]
Enable=0
Time=10
# Callsign=

[DMR Id Lookup]
File=/opt/data-files/DMRIds.dat
Time=24

[NXDN Id Lookup]
File=/opt/data-files/NXDN.csv
Time=24

[Modem]
# Valid values are "null", "uart", "udp", and (on Linux) "i2c"
Protocol=uart
# The port and speed used for a UART connection
# UARTPort=\\.\COM4
# UARTPort=/dev/ttyACM0
UARTPort=/dev/ttyAMA0
UARTSpeed=115200
#460800
# The port and address for an I2C connection
I2CPort=/dev/i2c
I2CAddress=0x22
# IP parameters for UDP connection
ModemAddress=192.168.2.100
ModemPort=3334
LocalAddress=192.168.2.1
LocalPort=3335

TXInvert=1
RXInvert=0
PTTInvert=0
TXDelay=100
RXOffset=0
TXOffset=0
DMRDelay=0
RXLevel=50
TXLevel=50
RXDCOffset=0
TXDCOffset=0
RFLevel=50
# CWIdTXLevel=50
# D-StarTXLevel=50
DMRTXLevel=50
YSFTXLevel=50
# P25TXLevel=50
# NXDNTXLevel=50
# M17TXLevel=50
# POCSAGTXLevel=50
# FMTXLevel=50
# AX25TXLevel=50
RSSIMappingFile=RSSI.dat
UseCOSAsLockout=0
Trace=0
Debug=0

[Transparent Data]
Enable=0
RemoteAddress=127.0.0.1
RemotePort=40094
LocalPort=40095
# SendFrameType=0

[D-Star]
Enable=0
Module=C
SelfOnly=0
AckReply=1
AckTime=750
AckMessage=0
ErrorReply=1
RemoteGateway=0
# ModeHang=10
WhiteList=

[DMR]
Enable=1
Beacons=0
BeaconInterval=60
BeaconDuration=3
ColorCode=1
SelfOnly=0
EmbeddedLCOnly=1
DumpTAData=0
# Prefixes=234,235
# Slot1TGWhiteList=
# Slot2TGWhiteList=
CallHang=3
TXHang=4
# ModeHang=10
# OVCM Values, 0=off, 1=rx_on, 2=tx_on, 3=both_on, 4=force off
# OVCM=0

[System Fusion]
Enable=1
LowDeviation=0
SelfOnly=0
TXHang=4
RemoteGateway=0
# ModeHang=10

[P25]
Enable=0
NAC=293
SelfOnly=0
OverrideUIDCheck=0
RemoteGateway=0
TXHang=5
# ModeHang=10

[NXDN]
Enable=0
RAN=1
SelfOnly=0
RemoteGateway=0
TXHang=5
# ModeHang=10

[M17]
Enable=0
CAN=0
SelfOnly=0
TXHang=5
# ModeHang=10

[POCSAG]
Enable=0
Frequency=439987500

[FM]
Enable=0
# Callsign=G4KLX
CallsignSpeed=20
CallsignFrequency=1000
CallsignTime=10
CallsignHoldoff=0
CallsignHighLevel=50
CallsignLowLevel=20
CallsignAtStart=1
CallsignAtEnd=1
CallsignAtLatch=0
RFAck=K
ExtAck=N
AckSpeed=20
AckFrequency=1750
AckMinTime=4
AckDelay=1000
AckLevel=50
# Timeout=180
TimeoutLevel=80
CTCSSFrequency=88.4
CTCSSThreshold=30
# CTCSSHighThreshold=30
# CTCSSLowThreshold=20
CTCSSLevel=20
KerchunkTime=0
HangTime=7
# AccessMode values are:
#   0 - Carrier access with COS
#   1 - CTCSS only access without COS
#   2 - CTCSS only access with COS
#   3 - CTCSS only access with COS to start, then carrier access with COS
AccessMode=1
# LinkMode=1 to remove almost all of the logic control
LinkMode=0
COSInvert=0
NoiseSquelch=0
SquelchThreshold=30
# SquelchHighThreshold=30
# SquelchLowThreshold=20
RFAudioBoost=1
MaxDevLevel=90
ExtAudioBoost=1
# ModeHang=10

[AX.25]
Enable=0
TXDelay=300
RXTwist=6
SlotTime=30
PPersist=128
Trace=1

[D-Star Network]
Enable=0
#LocalAddress=127.0.0.1
#LocalPort=20011
GatewayAddress=127.0.0.1
GatewayPort=20010
# ModeHang=3
Debug=0

[DMR Network]
Enable=1
#########################################################
#       Gateway   -    Multiples Server - DMRGateway    #
#########################################################
#Type=Gateway
#LocalAddress=127.0.0.1
#LocalPort=62034
#RemoteAddress=127.0.0.1
#RemotePort=62033
#
#########################################################
#       Direct    -     single server                   #
#########################################################
Type=Direct
RemoteAddress=198.211.36.245
RemotePort=62031
Password=passw0rd
#
#########################################################
Jitter=500
Slot1=1
Slot2=1
# No active linea de Option para TG estaticos, si utiliza BM,TGIF,DMR-Central
# Puede activar linea de option de selfcare FDMR-Mon y colocar su propia contraseña o 
# utilizar linea de options con opciones de tg estaticos
Options=PASS=abc123
#Options=TS2=714,7144;DIAL=0;VOICE=0;LANG=es_ES;SINGLE=0;TIMER=10;
# ModeHang=3
Debug=0


[System Fusion Network]
Enable=1
#########################################################
#   Gateway mode  -    Multiples Server - DMRGateway    #
#########################################################
#
#LocalAddress=127.0.0.1
#LocalPort=3330
#GatewayAddress=127.0.0.1
#GatewayPort=4330
#
#########################################################
#      Direct mode   -    Single Server - DMRGateway    #
#########################################################
#
GatewayAddress=europelink.pa7lim.nl
GatewayPort=42000
#
##########################################################
# ModeHang=3
Debug=0

[P25 Network]
Enable=0
#LocalAddress=127.0.0.1
#LocalPort=32010
GatewayAddress=127.0.0.1
GatewayPort=42020
# ModeHang=3
Debug=0

[NXDN Network]
Enable=0
Protocol=Icom
#LocalAddress=127.0.0.1
#LocalPort=14021
GatewayAddress=127.0.0.1
GatewayPort=14020
# ModeHang=3
Debug=0

[M17 Network]
Enable=0
#LocalAddress=127.0.0.1
#LocalPort=17011
GatewayAddress=127.0.0.1
GatewayPort=17010
# ModeHang=3
Debug=0

[POCSAG Network]
Enable=0
#LocalAddress=127.0.0.1
#LocalPort=3800
GatewayAddress=127.0.0.1
GatewayPort=4800
# ModeHang=3
Debug=0

[FM Network]
Enable=0
# Protocol=USRP
#LocalAddress=127.0.0.1
#LocalPort=3810
GatewayAddress=127.0.0.1
GatewayPort=4810
PreEmphasis=1
DeEmphasis=1
TXAudioGain=1.0
RXAudioGain=1.0
# ModeHang=3
Debug=0

[AX.25 Network]
Enable=0
Port=/dev/ttyp7
Speed=9600
Debug=0

[TFT Serial]
# Port=modem
Port=/dev/ttyAMA0
Brightness=50

[HD44780]
Rows=2
Columns=16

# For basic HD44780 displays (4-bit connection)
# rs, strb, d0, d1, d2, d3
Pins=11,10,0,1,2,3

# Device address for I2C
I2CAddress=0x20

# PWM backlight
PWM=0
PWMPin=21
PWMBright=100
PWMDim=16

DisplayClock=1
UTC=0

[Nextion]
Port=modem
#Port=/dev/ttyAMA0
Brightness=50
DisplayClock=1
UTC=0
#Screen Layout: 0=G4KLX 2=ON7LDS
ScreenLayout=2
IdleBrightness=20

[OLED]
Type=3
Brightness=1
Invert=0
Scroll=0
Rotate=1
Cast=0
LogoScreensaver=0

[LCDproc]
Address=localhost
Port=13666
#LocalPort=13667
DimOnIdle=0
DisplayClock=1
UTC=0

[Lock File]
Enable=0
File=/tmp/MMDVM_Active.lck

[Remote Control]
Enable=0
Address=127.0.0.1
Port=7642


EOF
#
#################################################################################################################################
#                                                   MMDVMHost - Dashboard WebSocket
#################################################################################################################################
#web

apps=("python3-pip" "python3-websockets" "python3-gpiozero" "python3-psutil" "python3-serial")

for app in "${apps[@]}"
do
    # Verificar apps
    if ! dpkg -s "$app" >/dev/null 2>&1; then
        # app no instalada
        sudo apt-get install -y "$app"
    else
        # app ya instalada
        echo "$app ya instalada"
    fi
done

if ! command -v ansi2html &> /dev/null; then
    pip3 install ansi2html
fi
sudo adduser --system --no-create-home --group mmdvm

if [ -d "/opt/MMDVMHost-Websocketboard" ]
then
   rm -r /opt/MMDVMHost-Websocketboard
 #echo "found file"

fi

cd /opt/
git clone --recurse-submodules -j8 https://github.com/dg9vh/MMDVMHost-Websocketboard
sudo chown -R mmdvm:mmdvm /opt/MMDVMHost-Websocketboard

#
sudo sed '264 a <!--' -i /opt/MMDVMHost-Websocketboard/html/index.html 
sudo sed '266 a -->' -i /opt/MMDVMHost-Websocketboard/html/index.html 

sudo sed -i "s/\`Custom-Headline-Text\`/\`Websocket-Based\`/g"  /opt/MMDVMHost-Websocketboard/html/js/config.js
sudo sed -i "s/about = 1/about = 0/g" /opt/MMDVMHost-Websocketboard/html/js/config.js
sudo sed -i "s/currtx = 1/currtx = 0/g" /opt/MMDVMHost-Websocketboard/html/js/config.js
sudo sed -i "s/qso = 1/qso = 0/g" /opt/MMDVMHost-Websocketboard/html/js/config.js
sudo sed -i "s/dapnet = 1/dapnet = 0/g" /opt/MMDVMHost-Websocketboard/html/js/config.js

sudo sed -i "s/DGIdGateway/YSFGateway/g" /opt/MMDVMHost-Websocketboard/logtailer.ini
sudo sed -i "s/BinaryName5/#BinaryName5/g" /opt/MMDVMHost-Websocketboard/logtailer.ini
sudo sed -i 's/Logdir=\/mnt\/ramdisk/Logdir=\/var\/log\/mmdvmh/' /opt/MMDVMHost-Websocketboard/logtailer.ini
sudo sed -i 's/5678/5679/' /opt/MMDVMHost-Websocketboard/logtailer.ini
sudo sed -i 's/Filerotate=True/Filerotate=False/' /opt/MMDVMHost-Websocketboard/logtailer.ini
sudo sed -i 's/etc\/MMDVM/opt\/MMDVMHost/' /opt/MMDVMHost-Websocketboard/logtailer.ini
sudo sed -i 's/usr\/local\/bin/opt\/MMDVMHost/' /opt/MMDVMHost-Websocketboard/logtailer.ini
sudo sed -i 's/Prefix=MMDVM/Prefix=MMDVMH/' /opt/MMDVMHost-Websocketboard/logtailer.ini
variable2=$(date +'%Y' | tail -c 5)
sudo sed -i "s/switch theme.*/switch theme<\/a><\/span>  <\/div> <p style=\"text-align: center;\"><span class=\"text-muted\"><a title=\"Raspbian Proyect by HP3ICC © 2018-$variable2\" target=\"_blank\" href=https:\/\/gitlab.com\/hp3icc\/emq-TE1\/>Proyect: emq-TE1+<\/a><\/span>/g" /opt/MMDVMHost-Websocketboard/html/index.html
#
cd /opt/MMDVMHost-Websocketboard/html/
sudo sed -i 's/5678/5679/' /opt/MMDVMHost-Websocketboard/html/index.html
sudo sed -i 's/<span class="navbar-brand float:center"><script type="text\/javascript">document.write(customHeadlineText);<\/script><\/span>/<span style="color: #808080;" class="navbar-brand float:center"><script type="text\/javascript">document.write(customHeadlineText);<\/script><\/span>/g'  /opt/MMDVMHost-Websocketboard/html/index.html
sudo sed -i 's/<script type="text\/javascript">document.write(customText);<\/script>/<span style="color: #3cff33;"><script type="text\/javascript">document.write(customText);<\/script>/g'  /opt/MMDVMHost-Websocketboard/html/index.html
sudo sed -i 's/This is an example/MMDVMHost Dashboard/' /opt/MMDVMHost-Websocketboard/html/js/config.js
sudo sed -i 's/you can use all html-tags and multiline-text./MMDVMHost-Websocketboard by DG9VH/' /opt/MMDVMHost-Websocketboard/html/js/config.js
sudo sed -i 's/<span class="navbar-brand float:right">Websocket-Based<\/span>/<span style="color: #808080;" <li>Temperature: <span id="cputemp"><\/span> °C<\/li>/g'  /opt/MMDVMHost-Websocketboard/html/index.html
sudo sed -i 's/<a class="navbar-brand" href="#">MMDVM-Dashboard by DG9VH<\/a>/<h6 style="text-align: center;"><span style="color: #808080;"><a style="color: #808080;" href="https:\/\/github.com\/hp3icc\/emq-TE1ws\/" target="_blank">emq-te1ws Raspbian Proyect by hp3icc<\/a> copyright 2018-YK00<\/span><\/h6>/g'  /opt/MMDVMHost-Websocketboard/html/index.html
variable2=$(date +'%Y' | tail -c 5)
sudo sed -i "s/YK00/$variable2/g"  /opt/MMDVMHost-Websocketboard/html/index.html

############################
cat > /lib/systemd/system/http.server-mmdvmh.service <<- "EOF"
[Unit]
Description=Python3 http.server.mmdvmhost
After=network.target

[Service]
#User=root
#ExecStartPre=/bin/sleep 5
# Modify for different location of Python3 or other port
ExecStart=/usr/bin/python3 -m http.server 80 --directory /opt/MMDVMHost-Websocketboard/html
Restart=on-failure

[Install]
WantedBy=multi-user.target


EOF
#
cat > /lib/systemd/system/logtailer-mmdvmh.service <<- "EOF"
[Unit]
Description=Python3 logtailer for MMDVMDash
After=network.target

[Service]
#Type=simple
#User=mmdvm
#Group=mmdvm
#Restart=always
#ExecStartPre=/bin/sleep 5
# Modify for different location of Python3 or other port
WorkingDirectory=/opt/MMDVMHost-Websocketboard/
ExecStart=/usr/bin/python3 /opt/MMDVMHost-Websocketboard/logtailer.py
Restart=on-failure

[Install]
WantedBy=multi-user.target


EOF
#
#####################################################################################################################

cat > /opt/data-file.sh <<- "EOFXC"
#!/bin/bash
#########################################################
#                                                       #
#              HostFilesUpdate.sh Updater               #
#                                                       #
#      Written for Pi-Star (http://www.pistar.uk/)      #
#               By Andy Taylor (MW0MWZ)                 #
#                                                       #
#                     Version 2.6                       #
#                                                       #
#   Based on the update script by Tony Corbett G0WFV    #
#                                                       #
#########################################################

# Check that the network is UP and die if its not
#if [ "$(expr length `hostname -I | cut -d' ' -f1`x)" == "1" ]; then
#	exit 0
#fi

# Get the Pi-Star Version
#pistarCurVersion=$(awk -F "= " '/Version/ {print $2}' /etc/pistar-release)

APRSHOSTS=/opt/data-files/APRSHosts.txt
DCSHOSTS=/opt/data-files/DCS_Hosts.txt
DExtraHOSTS=/opt/data-files/DExtra_Hosts.txt
DMRIDFILE=/opt/data-files/DMRIds.dat
DMRHOSTS=/opt/data-files/DMR_Hosts.txt
DPlusHOSTS=/opt/data-files/DPlus_Hosts.txt
P25HOSTS=/opt/data-files/P25Hosts.txt
M17HOSTS=/opt/data-files/M17Hosts.txt
YSFHOSTS=/opt/data-files/YSFHosts.txt
FCSHOSTS=/opt/data-files/FCSHosts.txt
XLXHOSTS=/opt/data-files/XLXHosts.txt
NXDNIDFILE=/opt/data-files/NXDN.csv
NXDNHOSTS=/opt/data-files/NXDNHosts.txt
TGLISTBM=/opt/data-files/TGList_BM.txt
TGLISTDMR=/opt/data-files/TGList-DMR.txt
TGLISTP25=/opt/data-files/TGList_P25.txt
TGLISTNXDN=/opt/data-files/TGList_NXDN.txt
TGLISTYSF=/opt/data-files/TGList_YSF.txt

# How many backups
FILEBACKUP=1

# Check we are root
if [ "$(id -u)" != "0" ];then
echo "This script must be run as root" 1>&2
exit 1
fi

# Create backup of old files
if [ ${FILEBACKUP} -ne 0 ]; then
cp ${APRSHOSTS} ${APRSHOSTS}.$(date +%Y%m%d)
cp ${DCSHOSTS} ${DCSHOSTS}.$(date +%Y%m%d)
cp ${DExtraHOSTS} ${DExtraHOSTS}.$(date +%Y%m%d)
cp ${DMRIDFILE} ${DMRIDFILE}.$(date +%Y%m%d)
cp ${DMRHOSTS} ${DMRHOSTS}.$(date +%Y%m%d)
cp ${DPlusHOSTS} ${DPlusHOSTS}.$(date +%Y%m%d)
cp ${P25HOSTS} ${P25HOSTS}.$(date +%Y%m%d)
cp ${M17HOSTS} ${M17HOSTS}.$(date +%Y%m%d)
cp ${YSFHOSTS} ${YSFHOSTS}.$(date +%Y%m%d)
cp ${FCSHOSTS} ${FCSHOSTS}.$(date +%Y%m%d)
cp ${XLXHOSTS} ${XLXHOSTS}.$(date +%Y%m%d)
cp ${NXDNIDFILE} ${NXDNIDFILE}.$(date +%Y%m%d)
cp ${NXDNHOSTS} ${NXDNHOSTS}.$(date +%Y%m%d)
cp ${TGLISTBM} ${TGLISTBM}.$(date +%Y%m%d)
        cp ${TGLISTDMR} ${TGLISTDMR}.$(date +%Y%m%d)
cp ${TGLISTP25} ${TGLISTP25}.$(date +%Y%m%d)
cp ${TGLISTNXDN} ${TGLISTNXDN}.$(date +%Y%m%d)
cp ${TGLISTYSF} ${TGLISTYSF}.$(date +%Y%m%d)
        
fi

# Prune backups
FILES="${APRSHOSTS}
${DCSHOSTS}
${DExtraHOSTS}
${DMRIDFILE}
${DMRHOSTS}
${DPlusHOSTS}
${P25HOSTS}
${M17HOSTS}
${YSFHOSTS}
${FCSHOSTS}
${XLXHOSTS}
${NXDNIDFILE}
${NXDNHOSTS}
${TGLISTBM}
${TGLISTDNR}
${TGLISTP25}
${TGLISTNXDN}
${TGLISTYSF}"

for file in ${FILES}
do
  BACKUPCOUNT=$(ls ${file}.* | wc -l)
  BACKUPSTODELETE=$(expr ${BACKUPCOUNT} - ${FILEBACKUP})
  if [ ${BACKUPCOUNT} -gt ${FILEBACKUP} ]; then
for f in $(ls -tr ${file}.* | head -${BACKUPSTODELETE})
do
rm $f
done
  fi
done

# Generate Host Files
curl --fail -o ${APRSHOSTS} -s http://www.pistar.uk/downloads/APRS_Hosts.txt --user-agent "Pi-Star_${pistarCurVersion}"
curl --fail -o ${DCSHOSTS} -s http://www.pistar.uk/downloads/DCS_Hosts.txt --user-agent "Pi-Star_${pistarCurVersion}"
curl --fail -o ${DMRHOSTS} -s http://www.pistar.uk/downloads/DMR_Hosts.txt --user-agent "Pi-Star_${pistarCurVersion}"
if [ -f /etc/hostfiles.nodextra ]; then
  # Move XRFs to DPlus Protocol
  curl --fail -o ${DPlusHOSTS} -s http://www.pistar.uk/downloads/DPlus_WithXRF_Hosts.txt --user-agent "Pi-Star_${pistarCurVersion}"
  curl --fail -o ${DExtraHOSTS} -s http://www.pistar.uk/downloads/DExtra_NoXRF_Hosts.txt --user-agent "Pi-Star_${pistarCurVersion}"
else
  # Normal Operation
  curl --fail -o ${DPlusHOSTS} -s http://www.pistar.uk/downloads/DPlus_Hosts.txt --user-agent "Pi-Star_${pistarCurVersion}"
  curl --fail -o ${DExtraHOSTS} -s http://www.pistar.uk/downloads/DExtra_Hosts.txt --user-agent "Pi-Star_${pistarCurVersion}"
fi
curl --fail -o ${DMRIDFILE} -s http://www.pistar.uk/downloads/DMRIds.dat --user-agent "Pi-Star_${pistarCurVersion}"
curl --fail -o ${P25HOSTS} -s http://www.pistar.uk/downloads/P25_Hosts.txt --user-agent "Pi-Star_${pistarCurVersion}"
curl --fail -o ${M17HOSTS} -s http://www.pistar.uk/downloads/M17_Hosts.txt --user-agent "Pi-Star_${pistarCurVersion}"
curl --fail -o ${YSFHOSTS} -s http://www.pistar.uk/downloads/YSF_Hosts.txt --user-agent "Pi-Star_${pistarCurVersion}"
curl --fail -o ${FCSHOSTS} -s http://www.pistar.uk/downloads/FCS_Hosts.txt --user-agent "Pi-Star_${pistarCurVersion}"
#curl --fail -s http://www.pistar.uk/downloads/USTrust_Hosts.txt --user-agent "Pi-Star_${pistarCurVersion}" >> ${DExtraHOSTS}
curl --fail -o ${XLXHOSTS} -s http://www.pistar.uk/downloads/XLXHosts.txt --user-agent "Pi-Star_${pistarCurVersion}"
curl --fail -o ${NXDNIDFILE} -s http://www.pistar.uk/downloads/NXDN.csv --user-agent "Pi-Star_${pistarCurVersion}"
curl --fail -o ${NXDNHOSTS} -s http://www.pistar.uk/downloads/NXDN_Hosts.txt --user-agent "Pi-Star_${pistarCurVersion}"
curl --fail -o ${TGLISTBM} -s http://www.pistar.uk/downloads/TGList_BM.txt --user-agent "Pi-Star_${pistarCurVersion}"
curl --fail -o ${TGLISTDMR} -s http://www.pistar.uk/downloads/TGList_BM.txt --user-agent "Pi-Star_${pistarCurVersion}"
curl --fail -o ${TGLISTP25} -s http://www.pistar.uk/downloads/TGList_P25.txt --user-agent "Pi-Star_${pistarCurVersion}"
curl --fail -o ${TGLISTNXDN} -s http://www.pistar.uk/downloads/TGList_NXDN.txt --user-agent "Pi-Star_${pistarCurVersion}"
curl --fail -o ${TGLISTYSF} -s http://www.pistar.uk/downloads/TGList_YSF.txt --user-agent "Pi-Star_${pistarCurVersion}"

# If there is a DMR Over-ride file, add it's contents to DMR_Hosts.txt
if [ -f "/root/DMR_Hosts.txt" ]; then
cat /root/DMR_Hosts.txt >> ${DMRHOSTS}
fi

# Add custom YSF Hosts
if [ -f "/root/YSFHosts.txt" ]; then
cat /root/YSFHosts.txt >> ${YSFHOSTS}
fi

# Fix DMRGateway issues with brackets
if [ -f "/etc/dmrgateway" ]; then
sed -i '/Name=.*(/d' /etc/dmrgateway
sed -i '/Name=.*)/d' /etc/dmrgateway
fi

# Add some fixes for P25Gateway

if [ -f "/root/P25Hosts.txt" ]; then
cat /root/P25Hosts.txt > /opt/data-files/P25HostsLocal.txt
fi

# Add local over-ride for M17Hosts
if [ -f "/root/M17Hosts.txt" ]; then
cat /root/M17Hosts.txt >> ${M17HOSTS}
fi

# Fix up new NXDNGateway Config Hostfile setup

if [ ! -f "/root/NXDNHosts.txt" ]; then
touch /root/NXDNHosts.txt
fi
if [ ! -f "/opt/data-files/NXDNHostsLocal.txt" ]; then
touch /opt/data-files/NXDNHostsLocal.txt
fi

# Add custom NXDN Hosts
if [ -f "/root/NXDNHosts.txt" ]; then
cat /root/NXDNHosts.txt > /opt/data-files/NXDNHostsLocal.txt
fi

# If there is an XLX over-ride
if [ -f "/root/XLXHosts.txt" ]; then
        while IFS= read -r line; do
                if [[ $line != \#* ]] && [[ $line = *";"* ]]
                then
                        xlxid=`echo $line | awk -F  ";" '{print $1}'`
xlxip=`echo $line | awk -F  ";" '{print $2}'`
                        #xlxip=`grep "^${xlxid}" /opt/data-files/XLXHosts.txt | awk -F  ";" '{print $2}'`
xlxroom=`echo $line | awk -F  ";" '{print $3}'`
                        xlxNewLine="${xlxid};${xlxip};${xlxroom}"
                        /bin/sed -i "/^$xlxid\;/c\\$xlxNewLine" /opt/data-files/XLXHosts.txt
                fi
        done < /root/XLXHosts.txt
fi

# Yaesu FT-70D radios only do upper case
if [ -f "/etc/hostfiles.ysfupper" ]; then
sed -i 's/\(.*\)/\U\1/' ${YSFHOSTS}
sed -i 's/\(.*\)/\U\1/' ${FCSHOSTS}
fi

# Fix up ircDDBGateway Host Files on v4
if [ -d "/opt/data-files/ircddbgateway" ]; then
if [[ -f "/opt/data-files/ircddbgateway/DCS_Hosts.txt" && ! -L "/opt/data-files/ircddbgateway/DCS_Hosts.txt" ]]; then
rm -rf /opt/data-files/ircddbgateway/DCS_Hosts.txt
ln -s /opt/data-files/DCS_Hosts.txt /opt/data-files/ircddbgateway/DCS_Hosts.txt
fi
if [[ -f "/opt/data-files/ircddbgateway/DExtra_Hosts.txt" && ! -L "/opt/data-files/ircddbgateway/DExtra_Hosts.txt" ]]; then
rm -rf /opt/data-files/ircddbgateway/DExtra_Hosts.txt
ln -s /opt/data-files/DExtra_Hosts.txt /opt/data-files/ircddbgateway/DExtra_Hosts.txt
fi
if [[ -f "/opt/data-files/ircddbgateway/DPlus_Hosts.txt" && ! -L "/opt/data-files/ircddbgateway/DPlus_Hosts.txt" ]]; then
rm -rf /opt/data-files/ircddbgateway/DPlus_Hosts.txt
ln -s /opt/data-files/DPlus_Hosts.txt /opt/data-files/ircddbgateway/DPlus_Hosts.txt
fi
if [[ -f "/opt/data-files/ircddbgateway/CCS_Hosts.txt" && ! -L "/opt/data-files/ircddbgateway/CCS_Hosts.txt" ]]; then
rm -rf /opt/data-files/ircddbgateway/CCS_Hosts.txt
ln -s /opt/data-files/CCS_Hosts.txt /opt/data-files/ircddbgateway/CCS_Hosts.txt
fi
fi

exit 0


EOFXC
###
chmod +x /opt/data-file.sh
mkdir /opt/data-files
chmod 777 /opt/data-files
sh /opt/data-file.sh

###############################################################################################################################


systemctl daemon-reload






